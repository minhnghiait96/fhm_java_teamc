create table  endUser (
    id  INT PRIMARY KEY,
    name TEXT,
    password TEXT 
);
create table roles(
    id int PRIMARY KEY,
    name TEXT
);
create table users_roles (
	id int  PRIMARY KEY,
    userID int REFERENCES endUser (id),
    roleID int REFERENCES roles (id)
);
create table fileXML(
    id INT PRIMARY KEY,
    name TEXT,
    timestamp TEXT,
    idUser INT REFERENCES endUser (id)
    );
create table fileXMLDetail(
	id  INT PRIMARY KEY,
    name TEXT,
    beginLine TEXT,
    endLine TEXT,
    priority INT,
    content TEXT,
    begincolumn INT,
    endcolumn INT,
    rule TEXT,
    ruleset TEXT ,
    package TEXT,
    class TEXT,
    externalInfoUrl TEXT,
    idFile INT REFERENCES fileXML (id)
)
;

INSERT INTO public.enduser(
	id, name, password)
	VALUES (1,'test','test');

INSERT INTO public.roles(
	id, name)
	VALUES (1,'ROLE_USER');

INSERT INTO public.users_roles(
	id, roleid, userid)
	VALUES (1, 1, 1);
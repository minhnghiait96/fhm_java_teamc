package com.example.PMDTeam3.webapp.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.PMDTeam3.webapp.entity.User;
import com.example.PMDTeam3.webapp.entity.UserRp;
import com.example.PMDTeam3.webapp.service.ServiceResult;
import com.example.PMDTeam3.webapp.service.UserService;
import com.example.PMDTeam3.webapp.service.ServiceResult.Status;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserControler {
	@Autowired
	UserService userService;

	@PostMapping("/login")
	public ResponseEntity<ServiceResult> login(@RequestBody User userRequest) {
		User user = (User) userService.getUserByName(userRequest.getName()).getData();

		if (user != null && user.getName().equals(userRequest.getName())
				&& user.getPassword().equals(userRequest.getPassword())) {
			ServiceResult serviceResult = new ServiceResult();
			User userRp = new User();
			serviceResult = (ServiceResult) userService.getUserByName(userRequest.getName());
			userRp = (User) serviceResult.getData();
			UserRp u = new UserRp(userRp.getId(), userRp.getName());
			serviceResult.setData(u);
			return new ResponseEntity<ServiceResult>(serviceResult, HttpStatus.OK);
		} else {
			ServiceResult result = new ServiceResult();
			if (null == user) {
				result.setStatus(Status.FAILED);
				result.setMessage("username not exist");
				return new ResponseEntity<ServiceResult>(result, HttpStatus.OK);
			}

			if (!user.getPassword().equals(userRequest.getPassword())) {
				result.setStatus(Status.FAILED);
				result.setMessage("wrong username or  password");
				return new ResponseEntity<ServiceResult>(result, HttpStatus.OK);
			}

			result.setStatus(Status.FAILED);
			result.setMessage("error");
			return new ResponseEntity<ServiceResult>(result, HttpStatus.OK);
		}
	}

	@GetMapping("/rest/test")
	public ResponseEntity<String> test() {
		return new ResponseEntity<String>("test", HttpStatus.OK);
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "logout";
	}
}
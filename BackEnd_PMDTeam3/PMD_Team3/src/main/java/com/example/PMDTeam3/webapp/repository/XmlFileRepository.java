package com.example.PMDTeam3.webapp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.PMDTeam3.webapp.entity.XmlFile;

public interface XmlFileRepository extends JpaRepository<XmlFile, Long> {
	Optional<XmlFile> findById(Long id);

}

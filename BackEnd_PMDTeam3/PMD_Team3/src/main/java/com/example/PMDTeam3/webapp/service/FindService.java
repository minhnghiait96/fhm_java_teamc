package com.example.PMDTeam3.webapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import com.example.PMDTeam3.webapp.repository.EndUserRepository;
import com.example.PMDTeam3.webapp.repository.ReportXMLRepository;
import com.example.PMDTeam3.webapp.repository.XmlFileRepository;

@Service
public class FindService {
 @Autowired
 ReportXMLRepository reportXMLRepository;
 @Autowired
 EndUserRepository endUserRepository;
 @Autowired
 XmlFileRepository xmlFileRepository;
 public ServiceResult findFileXmlDetailByContent(String content,long idfilexml){
	  ServiceResult result = new ServiceResult();
	 result.setData(reportXMLRepository.findContent(content,idfilexml)); 
	 return result;
 }
 
 public ServiceResult findFileXmlDetailByIdfile(long id,int page,int size) {
	 ServiceResult result = new ServiceResult();
	 try {
		 Pageable pageable = PageRequest.of(page, size);
		 result.setData(reportXMLRepository.findByIdfilexml(xmlFileRepository.findById(id).get(),pageable));
	} catch (Exception e) {
		result.setData("id file not found");
	}
	
	 return result;
 }

public ServiceResult findFileXmlDetailByPriorityPage(long idfilexml , String priority,int page,int size) {
	 ServiceResult result = new ServiceResult();
	 Pageable pageable = PageRequest.of(page, size);
	 result.setData(reportXMLRepository.findByPriorityPage(priority,xmlFileRepository.findById(idfilexml).get(),pageable));
	return result;
}
public ServiceResult findFileXmlDetailByPriority(long idfilexml ,String priority) {
	 ServiceResult result = new ServiceResult();
	 result.setData(reportXMLRepository.findByPriority(priority,xmlFileRepository.findById(idfilexml).get()));
	return result;
}
}

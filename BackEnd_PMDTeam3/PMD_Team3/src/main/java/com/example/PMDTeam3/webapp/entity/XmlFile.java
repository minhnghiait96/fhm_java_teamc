package com.example.PMDTeam3.webapp.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "xmlFile")
public class XmlFile {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column
	private String fileName;
	@OneToOne
	@JoinColumn(name = "dbFile")
	private DBFile dbFile;

	@OneToMany(mappedBy = "idfilexml")
	@JsonIgnore
	private Set<ReportXML> reportXML = new HashSet<>();
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public DBFile getDbFile() {
		return dbFile;
	}

	public void setDbFile(DBFile dbFile) {
		this.dbFile = dbFile;
	}

	public XmlFile(String fileName, DBFile dbFile) {
		super();
		this.fileName = fileName;
		this.dbFile = dbFile;
	}

	public XmlFile(String fileName, User iduser) {
		super();
		this.fileName = fileName;

	}

	public XmlFile(long id, String fileName, DBFile dbFile, Set<ReportXML> reportXML, User iduser) {
		super();
		this.id = id;
		this.fileName = fileName;
		this.dbFile = dbFile;
		this.reportXML = reportXML;
	}

	public XmlFile() {
		super();
	}

}

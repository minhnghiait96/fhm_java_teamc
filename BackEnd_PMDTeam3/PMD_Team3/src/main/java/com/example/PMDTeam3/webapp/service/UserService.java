package com.example.PMDTeam3.webapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.PMDTeam3.webapp.repository.EndUserRepository;
import com.example.PMDTeam3.webapp.entity.User;
import com.example.PMDTeam3.webapp.service.ServiceResult.Status;

@Service
@Transactional
public class UserService {
	@Autowired
	EndUserRepository userRepo;

	@Autowired
	private JwtService jwtService;

	public ServiceResult getUserByName(String name) {
		ServiceResult serviceResult = new ServiceResult();
		String result;
		System.out.println("name:" + name);
		System.out.println(userRepo.findByName(name));
		serviceResult.setData(userRepo.findByName(name));
		User user = (User) serviceResult.getData();
		if (serviceResult.getData() != null) {
			result = jwtService.generateTokenLogin(user.getName());
			serviceResult.setMessage(result);
			return serviceResult;
		} else {
			serviceResult.setStatus(Status.FAILED);
			serviceResult.setMessage("error");
			return serviceResult;
		}

	}
}

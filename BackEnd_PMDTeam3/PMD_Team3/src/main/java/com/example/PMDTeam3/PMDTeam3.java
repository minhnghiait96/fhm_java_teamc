package com.example.PMDTeam3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.example.PMDTeam3.webapp.repository")
@SpringBootApplication
public class PMDTeam3 extends SpringBootServletInitializer {
	public static void main(String args[]) {
		SpringApplication.run(PMDTeam3.class, args);
	}

}

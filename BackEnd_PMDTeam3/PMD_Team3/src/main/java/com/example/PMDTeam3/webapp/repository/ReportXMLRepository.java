package com.example.PMDTeam3.webapp.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.PMDTeam3.webapp.entity.ReportXML;
import com.example.PMDTeam3.webapp.entity.XmlFile;

@Repository
public interface ReportXMLRepository extends JpaRepository<ReportXML, Long>{
	
@Query(value="SELECT * FROM reportxml u WHERE u.idfile_xml=?1",nativeQuery = true)

	public List<ReportXML> findByXmlFile(long id);

	@Query(" SELECT f FROM ReportXML f WHERE f.priority = :priority and f.idfilexml = :idfilexml")
	List<ReportXML> findByPriorityPage(@Param("priority") String priority, @Param("idfilexml") XmlFile idfilexml,
			Pageable pageable);

	@Query(" SELECT f FROM ReportXML f WHERE f.priority = :priority and f.idfilexml = :idfilexml")
	List<ReportXML> findByPriority(@Param("priority") String priority, @Param("idfilexml") XmlFile idfilexml);


	@Query(value = "SELECT id, priority,external_info_url,class_affect,package_affect,ruleset,rule,end_column,begin_column,end_line,begin_line,idfile_xml,temp"
			+ "	FROM public.reportxml\r\n"
			+ "   where public.reportxml.temp  like %:temp% and idfile_xml = :idfile_xml ", nativeQuery = true)
	List<ReportXML> findContent(@Param("temp") String temp, @Param("idfile_xml") long idfile_xml);

@Query( "SELECT f FROM  ReportXML f   WHERE f.temp  like %:temp% and f.idfilexml = :idfilexml ")
List<ReportXML> findContent(@Param("temp") String temp,@Param("idfilexml") XmlFile idfilexml,Pageable pageable);


//public Object findByIdfile(XmlFile xmlFile, Pageable pageable);

	List<ReportXML> findByIdfilexml(XmlFile xmlFile, Pageable pageable);
}

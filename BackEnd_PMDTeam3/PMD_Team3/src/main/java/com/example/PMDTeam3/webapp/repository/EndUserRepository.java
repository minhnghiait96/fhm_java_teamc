package com.example.PMDTeam3.webapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.PMDTeam3.webapp.entity.User;

@Repository
public interface EndUserRepository extends JpaRepository<User, Long> {
	User findByName(String name);

	public User findUserByName(String name);
}

package com.example.PMDTeam3.webapp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="reportxml")
public class ReportXML {
	public ReportXML(long id, String priority, String externalInfoUrl, String classAffect, String packageAffect,
			String ruleset, String rule, String endColumn, String beginColumn, String endLine, String beginLine,
			XmlFile idfilexml) {
		super();
		this.id = id;
		this.priority = priority;
		this.externalInfoUrl = externalInfoUrl;
		this.classAffect = classAffect;
		this.packageAffect = packageAffect;
		this.ruleset = ruleset;
		this.rule = rule;
		this.endColumn = endColumn;
		this.beginColumn = beginColumn;
		this.endLine = endLine;
		this.beginLine = beginLine;
		this.idfilexml = idfilexml;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String priority;
	private String externalInfoUrl;
	private String classAffect;
	private String packageAffect;
	private String ruleset;
	private String rule;
	private String endColumn;
	private String beginColumn;
	private String endLine;
	private String beginLine;
	private String temp;
	
public String getTemp() {
		return temp;
	}
	public void setTemp(String temp) {
		this.temp = temp;
	}
@ManyToOne()
@JsonIgnore
@JoinColumn(name="idfile_xml")
private XmlFile idfilexml;

public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public ReportXML(String priority, String externalInfoUrl, String classAffect,
		String packageAffect, String ruleset, String rule, String endColumn, String beginColumn, String endLine,
		String beginLine, XmlFile idfilexml,String temp) {
	super();
	this.priority = priority;
	this.externalInfoUrl = externalInfoUrl;
	this.classAffect = classAffect;
	this.packageAffect = packageAffect;
	this.ruleset = ruleset;
	this.rule = rule;
	this.endColumn = endColumn;
	this.beginColumn = beginColumn;
	this.endLine = endLine;
	this.beginLine = beginLine;
	this.idfilexml = idfilexml;
	this.temp=temp;
}
public ReportXML(String priority, String externalInfoUrl, String classAffect, String packageAffect,
		String ruleset, String rule, String endColumn, String beginColumn, String endLine, String beginLine) {
	super();
	this.priority = priority;
	this.externalInfoUrl = externalInfoUrl;
	this.classAffect = classAffect;
	this.packageAffect = packageAffect;
	this.ruleset = ruleset;
	this.rule = rule;
	this.endColumn = endColumn;
	this.beginColumn = beginColumn;
	this.endLine = endLine;
	this.beginLine = beginLine;
}
public String getPriority() {
	return priority;
}
public void setPriority(String priority) {
	this.priority = priority;
}
public String getExternalInfoUrl() {
	return externalInfoUrl;
}
public void setExternalInfoUrl(String externalInfoUrl) {
	this.externalInfoUrl = externalInfoUrl;
}
public String getClassAffect() {
	return classAffect;
}
public void setClassAffect(String classAffect) {
	this.classAffect = classAffect;
}
public String getPackageAffect() {
	return packageAffect;
}
public void setPackageAffect(String packageAffect) {
	this.packageAffect = packageAffect;
}
public String getRuleset() {
	return ruleset;
}
public void setRuleset(String ruleset) {
	this.ruleset = ruleset;
}
public String getRule() {
	return rule;
}
public void setRule(String rule) {
	this.rule = rule;
}
public String getEndColumn() {
	return endColumn;
}
public void setEndColumn(String endColumn) {
	this.endColumn = endColumn;
}
public String getBeginColumn() {
	return beginColumn;
}
public void setBeginColumn(String beginColumn) {
	this.beginColumn = beginColumn;
}
public String getEndLine() {
	return endLine;
}
public void setEndLine(String endLine) {
	this.endLine = endLine;
}
public String getBeginLine() {
	return beginLine;
}
public void setBeginLine(String beginLine) {
	this.beginLine = beginLine;
}
public XmlFile getIdfilexml() {
	return idfilexml;
}
public void setIdfilexml(XmlFile idfilexml) {
	this.idfilexml = idfilexml;
}
public ReportXML() {
	super();
}


}

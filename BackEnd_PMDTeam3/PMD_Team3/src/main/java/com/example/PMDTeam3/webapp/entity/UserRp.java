package com.example.PMDTeam3.webapp.entity;

public class UserRp {
	private long id;
	private String name;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserRp(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public UserRp(String name) {
		super();
		this.name = name;
	}

	public UserRp() {
		super();
	}

}

package com.example.PMDTeam3.webapp.controller;

import java.awt.SystemColor;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.example.PMDTeam3.webapp.entity.DBFile;
import com.example.PMDTeam3.webapp.entity.ReportXML;
import com.example.PMDTeam3.webapp.entity.User;
import com.example.PMDTeam3.webapp.entity.XmlFile;
import com.example.PMDTeam3.webapp.repository.DBFileRepository;
import com.example.PMDTeam3.webapp.repository.EndUserRepository;
import com.example.PMDTeam3.webapp.repository.ReportXMLRepository;
import com.example.PMDTeam3.webapp.repository.XmlFileRepository;
import com.example.PMDTeam3.webapp.service.FindService;
import com.example.PMDTeam3.webapp.service.ServiceResult;

import net.sourceforge.pmd.PMD;

@CrossOrigin(origins = "*",allowedHeaders = "*")
@RestController
public class UploadController {
	// Save the uploaded file to this folder
	@Autowired
	private EndUserRepository enduserRepository;
	@Autowired 
	private ReportXMLRepository reportXMLRepository;
	  @Autowired
	   private XmlFileRepository xmlFileRepository;
	  @Autowired 
	  private DBFileRepository dbFileRepository;
	  @Autowired
	    private FindService findService;

	 static DBFile iddbf; 
	 static XmlFile imlFile;
	
	private static String UPLOADED_FOLDER = "D://upload//";
	private static final Logger logger = LoggerFactory.getLogger(UploadController.class);
	 
	  @RequestMapping(value="/uploadFile/{id}",method=RequestMethod.POST,consumes = "multipart/form-data")
	    public String singleFileUpload(@RequestParam("file")  MultipartFile file,@PathVariable long id
	                                   ) throws SAXException, IOException, ParserConfigurationException {
			Optional<User> user = enduserRepository.findById(id); 
//	        }
	        try {
	            // Get the file and save it somewhere
	            byte[] bytes = file.getBytes();
	            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
	            Files.write(path, bytes);	          
	        } catch (IOException e) {
	            e.printStackTrace();
	        }

	       
	        String filename=UPLOADED_FOLDER +file.getOriginalFilename();
	        DBFile dbFile = new DBFile(filename,user.get());
	        DBFile dbf =  dbFileRepository.save(dbFile);
	        iddbf = dbf;
	        exportXML(file.getOriginalFilename(),user.get().getId());
	        readXML(UPLOADED_FOLDER,file.getOriginalFilename());
	        return "uploadStatus";
	        
	    }
	 
	
	  @GetMapping(value ="/report/{id}", produces = "application/json")
	  public ResponseEntity<List<ReportXML>> findbyXML(@PathVariable Long id){  
		return new ResponseEntity<List<ReportXML>>(reportXMLRepository.findByXmlFile(id), HttpStatus.OK);
		  
	  }
	  public void exportXML(String fileName, Long id) {
		  String javaFile=fileName;
		  String xmlFile=javaFile.substring(0,javaFile.lastIndexOf('.'));
		  
		  String[] arguments = { "-d", UPLOADED_FOLDER+javaFile, "-f", "xml", "-R", UPLOADED_FOLDER+"rules.xml", "-r",
				  UPLOADED_FOLDER+xmlFile +".xml" };
	PMD.run(arguments);
	Optional<User> users = enduserRepository.findById(id); 
	XmlFile xmlFile2 = new XmlFile(xmlFile+".xml",iddbf);
	 imlFile= xmlFileRepository.save(xmlFile2);
	  }
	@RequestMapping(value="/report",method=RequestMethod.GET, produces = "application/json")
	  public ArrayList<ReportXML> readXML(String path,String fileName) throws SAXException, IOException, ParserConfigurationException {
		  ArrayList<ReportXML> reports = new ArrayList<>();
		  String javaFile=fileName;
		  String xmlFile=javaFile.substring(0,javaFile.lastIndexOf('.'));
		  File f = new File(path,xmlFile.toString()+".xml");
		  DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder buider = factory.newDocumentBuilder();
			Document doc = buider.parse(f);
			
			Element pmd = doc.getDocumentElement();
			NodeList files = pmd.getElementsByTagName("file");
			for (int i = 0; i < files.getLength(); i++) {
				Node file = files.item(i);
				NodeList violations = file.getChildNodes();
				for (int j = 0; j < violations.getLength(); j++) {
					Node node = violations.item(j);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element violation = (Element) node;
						String priority=violation.getAttribute("priority");
								violation.getTextContent().replaceAll("\n", "");
						String externalInfoUrl= violation.getAttribute("externalInfoUrl");
						String className=	violation.getAttribute("class");
						String packageName=	violation.getAttribute("package");
						String ruleSet=		violation.getAttribute("ruleset");
						String rule=	violation.getAttribute("rule");
						String endColumn=violation.getAttribute("endcolumn");
						String beginColumn=violation.getAttribute("begincolumn");
						String endLine	=violation.getAttribute("endline");
						String beginLine=violation.getAttribute("beginline");
						String temp =violation.getTextContent();
						System.out.println(temp);
						ReportXML report = new ReportXML(priority, externalInfoUrl, className, packageName, ruleSet, rule, endColumn, beginColumn, endLine, beginLine, imlFile,temp);
						reports.add(report);
						reportXMLRepository.save(report);
					}
				}
			}
			
		  return reports;
	  }
	@GetMapping("/getfile")
	public List<DBFile> getListDBFile(){
		return dbFileRepository.findAll();
	}
	 @RequestMapping(value = "/downloadFile/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	    public ResponseEntity<Object> downloadFile(@PathVariable long id, HttpServletResponse response) {
	 
	   
	        Resource resource = null;
	        Optional<XmlFile> xmlFile = xmlFileRepository.findById(id);
	        String fileName=xmlFile.get().getFileName();
	        System.out.println(fileName);
	        Path path = Paths.get(UPLOADED_FOLDER+xmlFile.get().getFileName());
	        		System.out.println(path);
	        try {
	            resource = new UrlResource(path.toUri());
	        } catch (MalformedURLException e) {
	            e.printStackTrace();
	        }
	 
	        if (resource.exists()) {
	            return ResponseEntity.ok()
	                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
	                    .body(resource);
	 
	           
	        } else {
	            return new ResponseEntity<Object>("File Not Found ", HttpStatus.OK);
	        }
	    }
	  @PostMapping("/findFileXMLDetailByContent/{idfilexml}")
	    public ResponseEntity<ServiceResult> findFileXMLDetailByContent(@RequestBody SeachFileXMLDetailRequest request,@PathVariable long idfilexml){
	    	  return new ResponseEntity<ServiceResult>(findService.findFileXmlDetailByContent(request.getContent(),idfilexml),HttpStatus.OK);
	    }	

	 @GetMapping("/getFileXMLDetailByFileXML/{idfilexml}/{page}/{size}")
	    public ResponseEntity<?> getFileXMLDetailByFileXML(@PathVariable long idfilexml,@PathVariable int page,@PathVariable int size) {
	    	 return ResponseEntity.ok(findService.findFileXmlDetailByIdfile(idfilexml,page,size).getData());
	    }

	 @GetMapping("/getFileXMLDetailByPriorityPage/{idfilexml}/{priority}/{page}/{size}")
	    public  ResponseEntity<?> getFileXMLDetailByPriorityPage(@PathVariable long idfilexml,@PathVariable String priority,@PathVariable int page,@PathVariable int size){
	    	 return ResponseEntity.ok(findService.findFileXmlDetailByPriorityPage(idfilexml,priority,page,size).getData());
	    }
	 @GetMapping("/getFileXMLDetailByPriority/{idfilexml}/{priority}")
	    public  ResponseEntity<?> getFileXMLDetailByPriority(@PathVariable long idfilexml,@PathVariable String priority){
		 return ResponseEntity.ok(findService.findFileXmlDetailByPriority(idfilexml,priority).getData());
	    }


}

package com.example.PMDTeam3.webapp.repository;


import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.PMDTeam3.webapp.entity.DBFile;

import com.example.PMDTeam3.webapp.entity.User;

@Repository
public interface DBFileRepository extends JpaRepository<DBFile, Long> {
	@Query(value = "SELECT * FROM DBFile u WHERE u.fileName=?1", nativeQuery = true)
	public DBFile findDBFileByFileName(String fileName);
	@Query("SELECT d FROM DBFile d WHERE d.filename like %:filename% and user=:user ")
	List<DBFile> findByFilename(@Param("filename") String filename,@Param("user") User user);

}

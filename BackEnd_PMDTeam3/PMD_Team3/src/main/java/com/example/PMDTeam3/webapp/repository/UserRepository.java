package com.example.PMDTeam3.webapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.PMDTeam3.webapp.entity.User;

@RestController
@RequestMapping("/h")
public interface UserRepository extends JpaRepository<User, Long> {

}

package com.example.PMDTeam3.webapp.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "dbfile")
public class DBFile implements Serializable{
	public static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column
	private String filename;

	@ManyToOne
	@JsonIgnore
	private User user;
	
	@OneToOne(mappedBy = "dbFile")
	@JsonIgnore
    private XmlFile xmlFile;
	@Column
	private String fileUrl;
	public String getFileUrl() {
		return fileUrl;
	}
	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}
	public XmlFile getXmlFile() {
		return xmlFile;
	}
	public void setXmlFile(XmlFile xmlFile) {
		this.xmlFile = xmlFile;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	} 
	public String getFilename() {
		return filename;
	}
	
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public DBFile(String filename, User user) {
		super();
		this.filename = filename;
		this.user = user;
	}
	public DBFile(String filename, User user,String fileUrl) {
		super();
		this.filename = filename;
		this.user = user;
		this.fileUrl = fileUrl;
	}
	public DBFile(String filename, User user, XmlFile xmlFile) {
		super();
		this.filename = filename;
		this.user = user;
		this.xmlFile = xmlFile;
	}
	public DBFile(long id, String filename, User user, XmlFile xmlFile) {
		super();
		this.id = id;
		this.filename = filename;
		this.user = user;
		this.xmlFile = xmlFile;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public DBFile() {
		super();
	}

	

}

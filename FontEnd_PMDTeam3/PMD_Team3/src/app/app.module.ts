import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppComponent } from './app.component';
import { UploadComponent } from './upload/upload.component';
import { LoginComponent } from './login/login.component';
import { ShowErrorComponent } from './show-error/show-error.component';
import { HttpClientModule }    from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
// import { LogoutComponent } from './logout/logout.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms';
import {AuthGuard, AuthLoginGuard} from './auth.guard'
const appRoutes: Routes = [
  { path: 'login', component: LoginComponent,canActivate:[AuthLoginGuard] },
  { path: 'upload',component: UploadComponent,canActivate:[AuthGuard]},
  {path:'showError/:id',component:ShowErrorComponent,canActivate:[AuthGuard]},

  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: '**', redirectTo: '/login', pathMatch: 'full'} 
];
@NgModule({
  declarations: [
    AppComponent,
    UploadComponent,
    LoginComponent,
    ShowErrorComponent,

    HeaderComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    FormsModule
  ],
  providers: [AuthGuard,AuthLoginGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AuthenticationService } from './service/authentication.service'

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class AuthGuard implements CanActivate {
  check = null;

  constructor(private auth: AuthenticationService,
    private myRoute: Router) {
  }

  canActivate(): boolean {
    this.check = sessionStorage.getItem('message');
    if (this.check !== null) {
      return true;
    } else {
      this.myRoute.navigate(["login"]);
      return false;
    }
  }

}

@Injectable()
export class AuthLoginGuard implements CanActivate {
  constructor(private myRoute: Router) {
  }
  check = sessionStorage.getItem('message');
  canActivate(): boolean {
    if (this.check != null) {
      this.myRoute.navigate(['upload']);
      return false;
    }
    else {
      return true;
    }


  }
}

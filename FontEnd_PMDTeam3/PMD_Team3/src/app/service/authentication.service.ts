import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private http: HttpClient, private router: Router) { }
  model: any = {}
  username: string;
  password: string;
  httpOptions = {
    headers: new HttpHeaders().set('Authorization', '' + sessionStorage.getItem('message'))
  };
  isUserLoggedIn() {
    let user = sessionStorage.getItem('message')
    console.log(!(user === null))
    return !(user === null)
  }
}

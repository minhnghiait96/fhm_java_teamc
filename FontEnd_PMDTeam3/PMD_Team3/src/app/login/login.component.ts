import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalidLogin = false;
  model: any = {};
  username: any;
  password: any;
  url = "http://10.88.31.51:8080/";
  httpOptions = {
    headers: new HttpHeaders().set('Authorization', '' + sessionStorage.getItem('message'))
  };
  constructor(private router: Router,
    private loginservice: AuthenticationService,
    private http: HttpClient) { }

  ngOnInit() {
  }
  checkLogin() {
    
    let result = this.http.post<any>(this.url + "login", {
      name: this.model.name,
      password: this.model.password
    }, { headers: new HttpHeaders().set('Authorization', '') }).subscribe(
      res => {
        if (res.status === "SUCCESS") {
          this.invalidLogin = false;
          this.router.navigate(['/upload']);
          sessionStorage.setItem(
            'id',
            res.data.id)
          sessionStorage.setItem('username', this.model.name)
          sessionStorage.setItem('message', res['message']);
        }
        else {
          this.invalidLogin = true;
        }
      }
    );

  }

}

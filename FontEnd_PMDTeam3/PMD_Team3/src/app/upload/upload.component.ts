import { Component, OnInit, ViewChild, Input, ElementRef } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { FormGroup } from "@angular/forms";
import { DataService } from '../service/data.service';
@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
    form: FormGroup;
    myFiles: string[] = [];
    sMsg: string = '';
    listFile: any = [];
    fileName: any;
    url = "http://10.88.31.51:8080/"
    httpOptions = {
        headers: new HttpHeaders().set('Authorization', '' + sessionStorage.getItem('message'))
    };
    @Input() multiple: boolean = false;
    @ViewChild('fileInput') inputEl: ElementRef;
    constructor(private http: HttpClient, private data: DataService) { }
    uploadFiles() {
        let inputEl: HTMLInputElement = this.inputEl.nativeElement;
        let fileCount: number = inputEl.files.length;
        const frmData = new FormData();
        if (fileCount > 0) {
            for (var i = 0; i < fileCount; i++) {
                frmData.append("file", inputEl.files.item(i));
            }
        }

        let id = sessionStorage.getItem('id');

        this.http.post(this.url + "uploadFile/" + id, frmData, this.httpOptions).subscribe(
            data => {
                // SHOW A MESSAGE RECEIVED FROM THE WEB API.
                this.sMsg = data as string;
            },
            (err: HttpErrorResponse) => {
            }
        );
        location.reload();
    }
    getListFile() {

        this.http.get(this.url + "getfile/", this.httpOptions).subscribe(res => {
            this.listFile = res
        }
        )

    }
    reportAFile(filename: string) {
        let catchuoi = filename.substring(9);
        let catchuoi1 = catchuoi.substring(catchuoi.lastIndexOf("."));
        catchuoi = catchuoi.replace(catchuoi1, "");
        this.data.changeMessage(catchuoi);

    }
    ngOnInit() {
        this.getListFile();
    }

}

import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Params, Router, ActivatedRoute } from '@angular/router'
import { DataService } from '../service/data.service';
@Component({
  selector: 'app-show-error',
  templateUrl: './show-error.component.html',
  styleUrls: ['./show-error.component.css']
})
export class ShowErrorComponent implements OnInit {
  listErrorPage: any;
  pages: any;
  listReport: any;;
  reportPage: any;
  listError: any;
  currentPage: any = 0;
  message: any;
  pager: any = {};
  pagedItems: any;
  id: any;
  url = "http://10.88.31.51:8080/";
  priority;
  httpOptions = {
    headers: new HttpHeaders().set('Authorization', '' + sessionStorage.getItem('message'))
  };
  constructor(private http: HttpClient, private route: ActivatedRoute, private data: DataService) {
  }
  getlogs(id) {
    this.http.get(this.url + "report/" + id, this.httpOptions).subscribe(res => {
      this.listReport = res;
      console.log(this.listReport)
      this.setPage();
    });
  }
  setPage() {
    if (this.priority !== '0') {
      this.http.get(this.url + "getFileXMLDetailByPriority/" + this.id + "/" + this.priority, this.httpOptions).subscribe(data => {
        this.listReport = data;
        console.log(this.listReport)
      }
      )
    }
  }

  fitlerPriority(prio) {
    this.priority = prio;
    this.http.get(this.url + "getFileXMLDetailByPriority/" + this.id + "/" + this.priority, this.httpOptions).subscribe(data => {
      this.listReport = data;
      this.setPage()
    })
  }
  downloadAFile(filename: string = null) {
    this.message = filename;

    const token = sessionStorage.getItem('message');
    const headers = new HttpHeaders().set('authorization', '' + token);
    this.http.get(this.url + "downloadFile/" + this.id, { responseType: 'blob' as 'json', headers }).subscribe(
      (response: any) => {
        let dataType = response.type;
        let binaryData = [];
        binaryData.push(response);
        let downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));
        if (filename)
          downloadLink.setAttribute('download', filename);
        document.body.appendChild(downloadLink);
        downloadLink.click();
      }
    )
  }

  ngOnInit() {
    this.priority = '0';
    this.data.currentMessage.subscribe(filename => console.log(this.message = filename));
    this.route.params.forEach((params: Params) => {
      let id = +params['id']; // (+) converts string 'id' to a number
      this.id = id;
      this.getlogs(id)
    });

  }
}